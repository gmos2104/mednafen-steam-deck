# Mednafen Config files for Steam Deck

Configs that I use for the [Mednafen](https://mednafen.github.io/) emulator on my [Steam Deck](https://www.steamdeck.com).

## Main Config

Main config file (mednafen.cfg) is mostly untouched except for the following configs:

- `cd.image_memcache 1`: Load the entire disk image to memory. Slightly increases initial loading time but allow us to load images from compressed files like zip.
- `sound.driver jack`: Use the jack audio backend. Steam Deck uses pipewire as a sound server and it's compatible with jack. I had problems using ALSA backend since it tries to grab the sound card for himself.
- `video.fs 1`: Start emulator in full-screen.

## Module Overrides

Module configurations will be overriden by their specific module config files.

### PSX (psx.cfg)

- Use Steam Deck resolution (1280x800)
- Steam Deck controls mapped to dualshock controller
- Scale resolution maintaining aspect ratio.
- Use GOAT filter to simulate CRT TV
